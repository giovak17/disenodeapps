// Barraza Andalon Kevin Giovanni 4A
// NOTA: Hay que ejecutar 'npm install' antes de ejecutar el programa

import axios from "axios";
import inquirer from "inquirer";


let json = [];
let option = 8;

async function getData() {
  try {
    const result = await axios.get("https://jsonplaceholder.typicode.com/todos");
    json = result.data;
    
  } catch (error) {
    json = [{ error: "There was an error"}];

  }
}

await getData();

function confirmation() {
  inquirer
  .prompt([
      {
        name: "confirmation",
        message: "Desea realizar otra accion? (s/n)",
        type: "input"

      }
    ])

  .then((answers) => {
      if (answers.confirmation.toLowerCase() == "s") {
        console.clear();
        menu();
      } else if (answers.confirmation.toLowerCase() == "n"){
        console.clear();
        console.log("Hasta luego!");
      } else {
        console.log("\nOpcion invalida.\n");
        confirmation();

      }
      
    });
  
}

async function menu() {
  console.log("\nNFL LISTA DE PENDIENTES\n");
  console.log("1. Lista de todos los pendientes (solo IDs)");
  console.log("2. Lista de todos los pendientes (IDS y Titles)");
  console.log("3. Lista de todos los pendientes sin resolver (ID y Title)");
  console.log("4. Lista de todos los pendientes resueltos (ID y Title)");
  console.log("5. Lista de todos los pendientes (IDs y UserID");
  console.log("6. Lista de todos los pendientes resueltos (ID y UserID)");
  console.log("7. Lista de todos los pendientes sin resolver (ID y UserID)");
  console.log("8. Salir\n");


  inquirer
    .prompt([
      {
        name: "option",
        message: "Elija una opcion: ",
        type: "number"
      }
    ])
    .then((answers) => {

      option = parseInt(answers.option);

      switch (option) {
        case 1:
          console.log("1. Lista de todos los pendientes (solo IDs)\n");

          for (let i = 0; i < json.length; i++) {
            console.log("ID: " + json[i].id);
          }
          confirmation();
          break;

        case 2:
          console.log("2. Lista de todos los pendientes (IDS y Titles)");

          for(let i = 0; i < json.length; i++){
            console.log("ID: " + json[i].id);
            console.log("Title: " + json[i].title + "\n");
          }
          
          confirmation();
          break;

        case 3:
          console.log("3. Lista de todos los pendientes sin resolver (ID y Title)");

          for (let i = 0; i < json.length; i++) {
            if (json[i].completed === false) {
              console.log("ID: " + json[i].id);
              console.log("Title: " + json[i].title + "\n");
              
            }
            
          }

          confirmation();
          break;

        case 4:
          console.log("4. Lista de todos los pendientes resueltos (ID y Title)");

          for (let i = 0; i < json.length; i++) {
            if (json[i].completed === true) {
              console.log("ID: " + json[i].id);
              console.log("Title: " + json[i].title + "\n");
              
            }
            
          }

          confirmation();
          break;

        case 5:
          console.log("5. Lista de todos los pendientes (IDs y UserID");

          for (let i = 0; i < json.length; i++) {
            console.log("ID: " + json[i].id);
            console.log("UserID " + json[i].userId + "\n");
          }

          confirmation();
          break;

        case 6:
          console.log("6. Lista de todos los pendientes resueltos (ID y UserID)");

          for (let i = 0; i < json.length; i++) {
            if (json[i].completed === true) {
              console.log("ID: " + json[i].id);
              console.log("UserID: " + json[i].userId + "\n");
            }
            
          }

          confirmation();
          break;

        case 7:
          console.log("7. Lista de todos los pendientes sin resolver (ID y UserID)");

          for (let i = 0; i < json.length; i++) {
            if (json[i].completed === false) {
              console.log("ID: " + json[i].id);
              console.log("UserID: " + json[i].userId + "\n");
            }
            
          }

          confirmation();
          break;

        case 8:
          console.log("Hasta luego!");
          break;


        default:
          console.clear();
          console.log("- OPCION NO VALIDA -");
          menu();
          break;
      }

      if (option != 8) {
        
      }


    });
}

console.clear();
menu();



